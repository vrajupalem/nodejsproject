const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');

const User = require('../../models/user');

const bcrypt = require('bcrypt');

const jwt = require('jsonwebtoken');

const bodyParser = require('body-parser');

router.post('/test', (req, res, next) => {
    console.log("in User.js: /test ");
    res.send("in User.js: /test ")
     
})

router.post('/signup', (req, res, next) => {
    // checking if email already exists
    console.log("in User.js: /signup ");
    console.log("req.body: " + req.body);
    console.log("req.body.email: " + req.body.email);
    
    User.find({email: req.body.email})
    .exec()
    .then(user => {
        if (user.length >= 1) {
            console.log("Email already exists")
            return res.status(409).json({
                message: 'Email already exists'
            });
        }
        else {
            // Create a new user
            console.log("Creating new user: user.js")
            console.log(" before bcrypt req.body.password: ", req.body.password)
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                console.log("bcrypt.hash : user.js")
                if(err) {
                    console.log("bcrypt error below: ")
                    console.log(err)
                    return res.status(500).json({
                        error: err
                    })
                } else {
                    // creating an instance of User model
                    console.log("creating an instance of User model : user.js")
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        email: req.body.email,
                        password: hash,
                        name: req.body.name,
                        user_type:'user'
                    });
                    console.log("Trying to save user : user.js")
                    console.group(user)
                    user.save()
                    .then(result => {
                        res.status(201).json({
                            message: 'User created successfully'
                        });
                    })
                    .catch(err => {
                        return res.status(500).json({
                            message:'Failure to save user',
                            error:err
                        });
                    });
                }
            })
            .catch(err => {
                console.log(err)
                return res.status(500).json({
                    message:'Failure in bcrypt.hash : user.js',
                    error:err
                });
            })
        }
    })
    /** 
    .catch(err => {
        console.log("Failure to find/create user: Error below:")
        console.log(err)
        return res.status(500).json({
            message:'Failure to find/create user',
            error:err
        });
    })
    */
   // Everything is successful if it reaches here
   console.log("Everything is successful if it reaches here in User.js: /signup ")
   //res.end();
})

router.post('/admin-signup', (req, res, next)  => {
    
    console.log("in User.js: /admin-signup ");
    console.log(req.body.email);
    User.find({email:req.body.email})
    .exec()
    .then(user => {
        if(user.length > 1) {
            return res.status(409).json({
                message: "Email already exists"
            });
        } else {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if(err) {
                    return res.status(500).json({
                        error:err
                    })
                } else {
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        email: req.body.email,
                        password: hash,
                        name: req.body.name,
                        user_type: 'admin'
                    })
                    user.save()
                    .then(result => {
                        res.status(201).json({
                            message:'Admin successfully created'
                        });
                    })
                    .catch(err => {
                        res.status(500).json({
                            message:'Admin creation failed',
                            error:err
                        });
                    });
                }
            })

        }
    })
})

router.post('/login', (req, res, next) => {
    console.log("in User.js: /login ");
    console.log(req.body.email);
    console.log(req.body.password);
    User.find({email:req.body.email})
    .exec()
    .then(user => {
        if(user.length < 1) {
            console.log("user.length < 1: user below")
            console.log(user)
            return res.status(401).json({
                message:'Auth failure'
            });
        }
        bcrypt.compare(req.body.password, user[0].password, (err,  result) => {
            if(err) {
                console.log("Auth failed in bcrypt: user.js /login")
                return res.status(401).json({
                    message:'Auth failed'
                });
            } 

            if (result) {
                const token = jwt.sign({
                    email:user[0].email,
                    userId:user[0]._id
                },
                'secret',
                {
                    expiresIn:"1h"
                }
                )
                console.log("user.js/login token: " + token)
                return res.status(200).json({
                    message:"Auth successful",
                    user_type:user[0].user_type,
                    token:token
                });
            } else {
                console.log("Auth failure: user.js/login")
                return res.status(401).json({
                    message:"Auth Failure"
                });
            }
            
        })
    })
    .catch(err => {
        return res.status(500).json({
            error:err
        });
    });
    // Everything is successful if it reaches here
   console.log("Everything is successful if it reaches here in User.js: /login ")
});

module.exports = router;