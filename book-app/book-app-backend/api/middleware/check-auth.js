const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        console.log("In checkauth.js")
        console.log(req.headers)
        const token = req.headers.authorization.split(" ")[1];
        console.log("token: ", token)
        const decoded = jwt.verify(token, 'secret');
        console.log("decoded: ", decoded)
        req.userData = decoded;
        console.log("calling next in check-auth.js")
        next();
    } catch(error) {
        return res.status(401).json({
            message:'Authorization failed in CheckAuth'
        });

    }
};