const express = require('express');

const app = express();

// Create first route in the application
// A simple express route
app.get('/', (req,res) => {
    console.log("This is our main endpoint! This is our book-app-backend service.")
    res.send("This is our main endpoint! This is our book-app-backend service.");
})
 
app.listen(process.env.port || 4000, function(){
    console.log("app.js now listening for requests on port 4000");
})


const bookRoutes = require('./api/routes/books');
const userRoutes = require('./api/routes/user');

const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb+srv://venu:7swARAs59@book-cls1-pljyc.mongodb.net/test?retryWrites=true&w=majority',
{useNewUrlParser: true, useUnifiedTopology: true}, () => {
    console.log("Connected to database successfully!")
});



app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());



app.use(function(req,res,next) {
    console.log('header-interceptor: Start setting headers.');
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers", "*"
    );
    if (req.method === 'OPTIONS') {
        console.log('header-interceptor: === OPTIONS.')
        var headers = {};
        headers["Access-Control-Allow-Methods"] = "POST, PATCH, GET, PUT, DELETE, OPTIONS";
        res.writeHead(200, headers);
        res.end();
        console.log('header-interceptor: Headers set.')
    }
    console.log("Calling next after headers set")
    next();
})



app.use('/user', userRoutes);
app.use('/books', bookRoutes);

// error handling middleware
app.use((err, req, res, next) => {
    console.log(err);
    res.status(422).send({error: err.message});
});

app.use((req,res,next) => {
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
})



